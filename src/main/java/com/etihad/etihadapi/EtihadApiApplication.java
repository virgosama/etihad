package com.etihad.etihadapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EtihadApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(EtihadApiApplication.class, args);
	}

}
