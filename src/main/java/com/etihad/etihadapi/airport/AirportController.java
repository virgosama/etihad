package com.etihad.etihadapi.airport;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("airports")
public class AirportController {

    @GetMapping("findAirport")
    public void findAirport(){

    }

    @GetMapping("getAirports")
    public void getAirports(){

    }
}
